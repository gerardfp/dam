package com.example.mvvm_login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mvvm_login.databinding.ActivityLoginGoogleBinding;
import com.example.mvvm_login.databinding.FragmentLoginBinding;

public class LoginGoogle extends DialogFragment {
    ActivityLoginGoogleBinding binding;
    private NavController navController;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return (binding = ActivityLoginGoogleBinding.inflate(inflater, container, false)).getRoot();


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        final LoginViewModel loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);
        LoginObserve loginObserve = new LoginObserve();

        binding.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = binding.editTextUsuario.getText().toString();
                String password = binding.editTextPassword.getText().toString();
                // Compruebo si hay campos vacios
                boolean camposVacios = new Utils().comprobarEditTexts(user, password, binding.editTextUsuario, binding.editTextPassword);
                if (!camposVacios) {
                    loginViewModel.comprobar(user, password);
                }

                navController.popBackStack();  // navegar hacia atras
            }
        });
    }
}