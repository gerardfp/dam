package com.example.mvvm_login;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mvvm_login.databinding.FragmentLoginBinding;

public class LoginFragment extends Fragment {
    FragmentLoginBinding binding;
    private NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentLoginBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);

        // en lugar de this => requireActivity()
        final LoginViewModel loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);

        LoginObserve loginObserve = new LoginObserve();

        // Click button login
        binding.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtengo usuario y contraseña
                String user = binding.editTextUsuario.getText().toString();
                String password = binding.editTextPassword.getText().toString();
                // Compruebo si hay algun campo vacio
                boolean camposVacios = new Utils().comprobarEditTexts(user, password, binding.editTextUsuario, binding.editTextPassword);
                if (!camposVacios)  {
                    loginViewModel.comprobar(user, password);
                }
                // Observo la barra de progreso
                loginObserve.observeProgressBar(loginViewModel, getViewLifecycleOwner(), view, binding.progressBar);
            }
        });

        // Observa el usuario y la contraseña
        loginObserve.observeUser(loginViewModel, getViewLifecycleOwner(), getContext());
        loginObserve.observerPassword(loginViewModel, getViewLifecycleOwner(), getContext());
        loginObserve.observeLoginValido(loginViewModel, getViewLifecycleOwner(), view);

        // Click button login with google
        binding.buttonLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                navController.navigate(R.id.loginGoogleFragment);
            }
        });

    }
}