package com.example.mvvm_login;

import android.widget.EditText;

public class Utils {
    // Compruebo si los campos de usuario o password estan vacios o no
    public boolean comprobarEditTexts(String user, String password, EditText editTextUser, EditText editTextPasswd) {
        boolean camposVacios = false;
        if (user.equals("")) {
            editTextUser.setError("Escriba un usuario");
            camposVacios = true;
        } if (password.equals("")) {
            editTextPasswd.setError("Escriba una contraseña");
            camposVacios = true;
        }
        return camposVacios;
    }


}
